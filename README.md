# Gearing

## Brushless Slot Car Gearing

Use this git repo to keep track of brushless slot car gearing.

You'll probably need a free gitlab account to maintain it, which may be done either in a Web browser
or locally on your computer.

The file gearing.ods is an attempt to capture gear ratios by motor by venue. It's useful information that
can shortcut some experimentation. 

Ultimately I'd like to match up this practical information with motor and track models that will also be found in this same repo.